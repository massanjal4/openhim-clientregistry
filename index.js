const medUtils = require('openhim-mediator-utils');
const apiConf = require('./config/config');
const mediatorConfig = require('./config/mediator');
// const console = require("console")

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

medUtils.registerMediator(apiConf.api, mediatorConfig, err => {
    if (err) {
        console.error('Failed to register this mediator, check your config');
        console.error(err.stack);
        process.exit(1);
    }
    apiConf.api.urn = mediatorConfig.urn;
    medUtils.fetchConfig(apiConf.api, (err, newConfig) => {
        console.info('Received initial config:', newConfig);
        config = newConfig;
        if (err) {
            console.info('Failed to fetch initial config');
            console.info(err.stack);
            process.exit(1);
        } else {
            console.info('Successfully registered mediator!');
            // let app = setupApp();
            // const server = app.listen(port, () => {
            //   let configEmitter = medUtils.activateHeartbeat(apiConf.api);
            //   configEmitter.on('error', error => {
            //     console.error(error);
            //     console.error('an error occured while trying to activate heartbeat');
            //   });
            //   configEmitter.on('config', newConfig => {
            //     console.info('Received updated config:', newConfig);
            //     // set new config for mediator
            //     config = newConfig;
            //   });
            //   callback(server);
            // });
        }
    });
});