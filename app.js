const express = require('express');
const parser = require('body-parser');
const request = require('request');
const medUtils = require('openhim-mediator-utils');
const apiConf = require('./config/config');
const mediatorConfig = require('./config/mediator');

const app = express();
const port = 3002;
const URL = 'http://192.168.43.83:8081/clientregistry/fhir/Location';

app.use(parser.json());

app.post('/facility', (req, res) => {
    result = {};
    result.resourceType = 'Location';
    result.name = req.body.facilityName;
    result.status = 'active';
    telecom = [];
    telecom.push({
        system: 'phone',
        value: req.body.phone,
    });
    telecom.push(
        {
            system: 'email',
            value: req.body.email,
        },
    );
    result.telecom = telecom;
    result.position = {};
    result.position.longitude = req.body.longitude;
    result.position.latitude = req.body.latitude;

    request.post(
        {
            headers: {'content-type': 'application/json'},
            url: URL,
            json: result,
        }, function (error, response, body) {
            console.log('error', error);
            console.log('response', response);
            console.log('body', body);
        },
    );

    console.log('result:', result);
    res.send(result);
});

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

medUtils.registerMediator(apiConf.api, mediatorConfig, err => {
    if (err) {
        console.error('Failed to register this mediator, check your config');
        console.error(err.stack);
        process.exit(1);
    }
    apiConf.api.urn = mediatorConfig.urn;
    medUtils.fetchConfig(apiConf.api, (err, newConfig) => {
        console.info('Received initial config:', newConfig);
        config = newConfig;
        if (err) {
            console.info('Failed to fetch initial config');
            console.info(err.stack);
            process.exit(1);
        } else {
            console.info('Successfully registered mediator!');
            app.listen(port, () => {
                console.log(`Listening to ${port}...`);
            });
        }
    });
});
